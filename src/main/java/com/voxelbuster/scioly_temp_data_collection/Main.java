package com.voxelbuster.scioly_temp_data_collection;

import com.fazecast.jSerialComm.SerialPort;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    private static float lastTemp = 0f;

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private static final File outFile = new File(System.currentTimeMillis() + "-data.csv");
    private static FileWriter fw;

    private static int dataCounter = 0;

    static {
        try {
            fw = new FileWriter(outFile);
            if (!outFile.exists()) {
                outFile.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final Runnable writeTask = new Runnable() {
        @Override
        public void run() {
            if (dataCounter >= maxDataPoints) return;
            try {
                fw.append(new StringBuilder().append(lastTemp).append(" ,").toString());
                fw.flush();
                System.out.println("Wrote temperature index " + dataCounter + ": " + lastTemp);
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataCounter++;
            if (dataCounter >= maxDataPoints) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private static final int maxDataPoints = 30;

    public static void main(String[] args) {
        SerialPort comPort = SerialPort.getCommPort("COM5");
        comPort.openPort();
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 500, 0);
        scheduler.scheduleAtFixedRate(writeTask, 1, 1, TimeUnit.SECONDS);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                System.out.println("Program being forcibly closed! Trying to save file.");
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
        while (true) {
            byte[] buffer = new byte[16];
            comPort.readBytes(buffer, buffer.length);
            System.out.println(new String(buffer));
            try {
                lastTemp = Float.parseFloat(new String(buffer));
            } catch (NumberFormatException nfe) {
                // ignore
            }
            if (dataCounter >= maxDataPoints) {
                break;
            }
        }
    }
}
